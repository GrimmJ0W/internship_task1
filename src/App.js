import PostContainer from "./components/PostContainer";
import posts from "./posts/posts";
import './style/style.css'
import PostEdit from "./components/PostEdit";
import {useState} from "react";
import Button from "./components/Button";

localStorage.setItem("allPosts", JSON.stringify(posts))

const userPost = JSON.parse(localStorage.getItem("allPosts"))

function App() {
    const [data, setData] = useState(userPost)
    const [postId, setPostId] = useState('');
    const [editPost, setEditPost] = useState(false)
    const [newPost, setCreatePost] = useState(false);
    const [showForm, setShowForm] = useState(false);

    const blankPost = {
        id: '',
        title: 'title',
        text: 'text',
        author: 'author'
    }

    function openPost(id) {
        if (newPost) return setEditPost(false)
        setPostId(id)
        setEditPost(true)
    }

    function createPost() {
        setCreatePost(!editPost)
    }

    function handleClose() {
        setCreatePost(false)
        setEditPost(false)
    }

    const postList = data.map((post) => {
        return <PostContainer key={post.id} post={post} findPostId={openPost}/>
    })

    function updatePost(post) {
        if (post.id === '') {
            const updatedPostList = [...data];
            let newId = 0;
            for (const props of updatedPostList) {
                if (props.id > newId) {
                    newId = props.id
                }
            }
            post.id = newId + 1;
            updatedPostList.push(post);
            return setData(updatedPostList)
        }

        const updatedPostList = [...data]
        updatedPostList.splice(post.id, 1, post)
        setData(updatedPostList)
        console.log(updatedPostList);
    }

    return (
        <div>
            <Button styleType="primary" onClick={createPost}>Create new post</Button>
            <div className="mainContainer">
                <div>
                    {postList}
                </div>
                {editPost &&
                <PostEdit updatePost={updatePost} postId={postId} handleClose={handleClose} postSource={data[postId]}/>}
                {newPost &&
                <PostEdit updatePost={updatePost} handleClose={handleClose} postSource={blankPost}/>}
            </div>
        </div>
    );
}

export default App;
