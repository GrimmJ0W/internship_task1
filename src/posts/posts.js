const posts = [
    {
        id: 0,
        title: "Post title1",
        author: "Непризнанный гений",
        text:'It is a long established fact that a reader will be distracted by the readable content of a page when ' +
            'looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution ' +
            'of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many' +
            'desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a ' +
            'search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions ' +
            'have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    },
    {
        id: 1,
        title: "Post title2",
        author: "Непризнанный гений",
        text:'It is a long established fact that a reader will be distracted by the readable content of a page when ' +
            'looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution ' +
            'of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many' +
            'desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a ' +
            'search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions ' +
            'have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    },
    {
        id: 2,
        title: "Post title3",
        author: "Непризнанный гений",
        text:'It is a long established fact that a reader will be distracted by the readable content of a page when ' +
            'looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution ' +
            'of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many' +
            'desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a ' +
            'search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions ' +
            'have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    },
    {
        id: 3,
        title: "Post title4",
        author: "Непризнанный гений",
        text:'It is a long established fact that a reader will be distracted by the readable content of a page when ' +
            'looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution ' +
            'of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many' +
            'desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a ' +
            'search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions ' +
            'have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    },
    {
        id: 4,
        title: "Post title5",
        author: "Непризнанный гений",
        text:'It is a long established fact that a reader will be distracted by the readable content of a page when ' +
            'looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution ' +
            'of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many' +
            'desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a ' +
            'search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions ' +
            'have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    },
    {
        id: 5,
        title: "Post title6",
        author: "Непризнанный гений",
        text:'It is a long established fact that a reader will be distracted by the readable content of a page when ' +
            'looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution ' +
            'of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many' +
            'desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a ' +
            'search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions ' +
            'have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    },
    {
        id: 6,
        title: "Post title7",
        author: "Непризнанный гений",
        text:'It is a long established fact that a reader will be distracted by the readable content of a page when ' +
            'looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution ' +
            'of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many' +
            'desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a ' +
            'search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions ' +
            'have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    },
    {
        id: 7,
        title: "Post title8",
        author: "Непризнанный гений",
        text:'It is a long established fact that a reader will be distracted by the readable content of a page when ' +
            'looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution ' +
            'of letters, as opposed to using \'Content here, content here\', making it look like readable English. Many' +
            'desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a ' +
            'search for \'lorem ipsum\' will uncover many web sites still in their infancy. Various versions ' +
            'have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).'
    }
]
export default posts;
