import React, {useEffect, useState} from 'react'
import Button from "../Button/index";
import "./style.css";

function PostContainer({post, findPostId}) {
    const [isTextCollapsed, setIsTextCollapsed] = useState(true);
    const [isTextMinimized, setTextPreview] = useState(true);

    function handleTextPreview() {
        setTextPreview(!isTextMinimized)
    }

    useEffect(() => {
        if (post.text.length < 138) {
            setIsTextCollapsed(false)
        }
    }, [post.text.length])

    return (
        <div className="post-container">
            <div className="post-title">
                <p>{post.title}</p>
            </div>
            <div className={`post-text ${isTextMinimized && 'text-hidden'}`}>
                <p>{post.text}</p>
            </div>
            <div className="post-container-footer">
                <p>{post.author}</p>
                <div style={{display: "flex"}}>
                    {
                        isTextCollapsed &&
                        <div>
                            {
                                isTextMinimized ?
                                    <Button styleType='secondary' onClick={handleTextPreview}>Load text</Button>
                                    : <Button styleType='secondary' onClick={handleTextPreview}>Minimize</Button>
                            }
                        </div>
                    }
                    <Button styleType='primary' onClick={() => findPostId(post.id)}>Edit post</Button>
                </div>
            </div>
        </div>
    );
}

export default PostContainer;
