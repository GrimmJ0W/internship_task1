import React from 'react';
import "./style.css";

function Button({styleType, children, onClick}) {
    return (
        <div className="button-container">
            <button className={`btn ${styleType}`} onClick={onClick}>{children}</button>
        </div>
    );
}

export default Button;
