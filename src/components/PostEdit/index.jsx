import React, {useState} from "react";
import Button from "../Button";
import TextArea from "../TextArea";
import "./style.css";
import Inputs from "../Inputs";


function PostEdit({postSource, handleClose, updatePost}) {
    const [editPost, setEditPost] = useState({...postSource})
    const [buttonState, setButtonState] = useState('disabled')

    function handleEditPostTitle(title) {
        const updatedPost = editPost;
        updatedPost.title = title;
        setEditPost(updatedPost);
        setButtonState('primary');
    }

    function handleEditPostAuthor(author) {
        const updatedPost = editPost;
        updatedPost.author = author;
        setEditPost(updatedPost);
        setButtonState('primary');
    }

    function handleEditPostText(text) {
        const updatedPost = editPost;
        updatedPost.text = text;
        setEditPost(updatedPost);
        setButtonState('primary');
    }


    function submit() {
        updatePost(editPost)
        handleClose()
    }

    return (
        <div className="postEdit-container">
            <p>Edit title</p>
            <Inputs onClick={handleEditPostTitle} text={editPost.title}/>
            <p>Edit text</p>
            <TextArea post={editPost} textUpdate={handleEditPostText}/>
            <p>Edit post author</p>
            <div className="postEdit-footer">
                <div>
                    <Inputs onClick={handleEditPostAuthor} text={editPost.author}/>
                </div>
                <div style={{display: " flex"}}>
                    <Button styleType="primary" onClick={handleClose}>DISCARD</Button>
                    <Button styleType={`${buttonState}`} onClick={submit}>SUBMIT</Button>
                </div>
            </div>
        </div>
    );
}

export default PostEdit;
