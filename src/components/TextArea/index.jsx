import React from "react";
import "./style.css";

function TextArea({post, textUpdate}) {
    return (
    <div className="text-area-container">
        <textarea className="textArea" defaultValue={post.text} onChange={(event)=>textUpdate(event.target.value)}/>
    </div>
        // <div className="text-are-container" >{post.text}</div>
);
}

export default TextArea;
