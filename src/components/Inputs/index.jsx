import React from "react";
import "./style.css"

function Inputs({text, onClick}) {
    return (
        <div className="input-container">
            <input className="input" placeholder={text} onChange={(event)=>onClick(event.target.value)}/>
        </div>
    );
}

export default Inputs;
